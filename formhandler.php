<?php
// do the validation using php
//http://api.jetsetrdv.com:80/index_api/subscribe?api_key=2fed08b3758b0fe08740de28648dda55&login=koushik752&pass=15089696&mail=koushik355%40gmal.com&sex=male&cherche1=1&year=1995&month=octorber&day=26&ip_adress=222.222.22.22&city=Kolkata&region=west%20bengal&countryObj=india

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $domain_url = "http://api.jetsetrdv.com/";
    $api_key = "2fed08b3758b0fe08740de28648dda55";
    $api_path = 'index_api/subscribe';
    $errors = [];
    // get the form fields
    function xssSafe($data)
    {
        return htmlentities(urlencode(trim($data)), ENT_QUOTES, 'UTF-8');
    }

    //flag variables
    $name = $email = $nickname = $password = $sex = $lookingFor = false;

    // validation goes here

    // vdalite name
    (!empty($_GET['name'])) ? $name = ucwords(xssSafe($_GET['name'])) : $errors[] = "Please enter your name";

    // validate email
    (filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) ? $email = xssSafe($_GET['email']) : $errors[] = "Please enter your valid email address";

    // validate the nickname
    (!empty($_GET['nickname'])) ? $nickname = ucwords(xssSafe($_GET['nickname'])) : $errors[] = "Please enter your nickname";

    // validate password
    (!empty($_GET['password'])) ? $password = xssSafe($_GET['password']) : $errors[] = "Please enter a password";

    // validate gender
    (isset($_GET['sex']) && xssSafe($_GET['sex']) != 'nothing') ? $sex = xssSafe($_GET['sex']) : $errors[] = "Please mention your sex";

    // validate looking for
    (isset($_GET['looking_for']) && xssSafe($_GET['looking_for']) != 'nothing') ? $lookingFor = xssSafe($_GET['looking_for']) : $errors[] = "Please mention what you're looking for";

    // pre supplied parameters
    $year = 1990;
    $month = 10;
    $day = 26;
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $id_city = 34817;
    $id_region = 485;
    $id_country = 64;

    // check if all values are provided
    if ($name && $email && $nickname && $sex && $password && $lookingFor) { // all values are provided

        //http://api.jetsetrdv.com:80/index_api/subscribe?api_key=2fed08b3758b0fe08740de28648dda55&login=koushik752&pass=15089696&mail=koushik355%40gmal.com&sex=male&cherche1=1&year=1995&month=octorber&day=26&ip_adress=222.222.22.22&city=Kolkata&region=west%20bengal&countryObj=india
        $domain_url = "http://api.jetsetrdv.com/";
        $api_key = "2fed08b3758b0fe08740de28648dda55";
        $api_path = "index_api/subscribe";
        $params_list = "login=$nickname&pass=$password&mail=$email&sex=$sex&cherche1=$lookingFor&year=$year&month=$month&day=$day&ip_address=$ip_address&city=$id_city&region=$id_region&countryObj=$id_country";
        $url_curl = $domain_url . $api_path . "?api_key=" . $api_key . "&" . $params_list;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url_curl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);

        // decode the json string into object
        $result = json_decode($result);

        if ($result->accepted == 1) { // accepted
            // redirect here
            header("Location: http://les-folles.com/");
        } elseif ($result->accepted == 0) { // not accepted
            echo $result->error;
        } else {
            echo "Something went wrong <br/>";
            echo $result;
        }

    } elseif (count($errors) > 0) { // not all values are provided
        foreach ($errors as $error) { // show the errors
            echo "$error <br/>";
        }
    }
}